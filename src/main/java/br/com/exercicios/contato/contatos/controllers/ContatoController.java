package br.com.exercicios.contato.contatos.controllers;

import br.com.exercicios.contato.contatos.DTOs.ContatoDTO;
import br.com.exercicios.contato.contatos.models.Contato;
import br.com.exercicios.contato.contatos.security.Usuario;
import br.com.exercicios.contato.contatos.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ContatoController {

    @Autowired
    ContatoService contatoService;

    @GetMapping
    @RequestMapping("contatos")
    public List<ContatoDTO> getContatos(@AuthenticationPrincipal Usuario usuario){
        return contatoService.getContatos(usuario);
    }

    @PostMapping
    @RequestMapping("contato")
    public Contato postContato(@RequestBody ContatoDTO contatoDTO, @AuthenticationPrincipal Usuario usuario) {
        return contatoService.postContato(contatoDTO,usuario);
    }

}
