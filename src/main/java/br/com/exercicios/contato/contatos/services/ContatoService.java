package br.com.exercicios.contato.contatos.services;

import br.com.exercicios.contato.contatos.DTOs.ContatoDTO;
import br.com.exercicios.contato.contatos.models.Contato;
import br.com.exercicios.contato.contatos.repository.ContatoRepository;
import br.com.exercicios.contato.contatos.security.Usuario;
import com.netflix.discovery.converters.Auto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;


    public List<ContatoDTO> getContatos(Usuario usuario) {
        try{
            List<Contato> allById = contatoRepository.findAllByProprietario(usuario.getNome());

            List<ContatoDTO> contatos = new ArrayList<>();
            for (Contato contato : allById) {
                ContatoDTO listarContato = new ContatoDTO(contato.getNome(), contato.getTelefone());
                contatos.add(listarContato);
            }
            return contatos;
        } catch (RuntimeException e){
            throw new RuntimeException("Não há contatos para este usuário");
        }
    }

    public Contato postContato(ContatoDTO contatoDTO, Usuario usuario) {
        Contato contatoDb = new Contato(usuario.getNome(), contatoDTO.getTelefone(),contatoDTO.getNome());
        return contatoRepository.save(contatoDb);
    }
}
