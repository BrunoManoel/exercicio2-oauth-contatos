package br.com.exercicios.contato.contatos.repository;


import br.com.exercicios.contato.contatos.models.Contato;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, String> {

    List<Contato> findAllByProprietario(String nome);
}
